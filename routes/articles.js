var express = require('express');
var router = express.Router();
var mysql = require('../config/db');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express Appp' });
});

var sql = 'select * from articles';
router.get('/get', function(req, res, next) {
  mysql.query(sql,function (err, result) {
    if(err)
    res.send({'error':true , 'message': err});
    else
    res.send({'success':true , 'data': result});
  });
});

router.post('/save', function(req, res, next) {
  mysql.query('INSERT INTO articles SET ?',req.body , function (err, result) {
    if(!err)
    res.json({'success': 'Article added successfully'});
    else
    res.json({'error': 'Something wrong' , 'message' : err});
  });
});

router.get('/getArticle', function(req, res, next) {
  var id = req.query.idd;
  mysql.query('select * from articles where idd = ?',[id] , function (err, result) {
    if(!err)
    res.json({'success': 'true' , 'data' : result});
    else
    res.json({'error': 'Something wrong' , 'message' : err});
  });
});



module.exports = router;
